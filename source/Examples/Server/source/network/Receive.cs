﻿using System;
using Asfw;

namespace server
{
    internal static partial class Network
    {
        internal static void PacketRouter()
        {
            // Any time new packets are made that are expected to be receieved
            // which in this case our server receieves clientpackets. Add it to here
            // and route it to the function handler via AddressOf. Do not include count
            // as the count name in the enumerators arent actually created in the network
            // as pointers.
            Socket.PacketId[(int)ClientPackets.FakeLogin] = Handle_FakeLogin;
            Socket.PacketId[(int)ClientPackets.Message] = Handle_Message;
        }

        internal static void Handle_FakeLogin(int index, ref byte[] data)
        {
            var buffer = new ByteStream(data);
            var user = buffer.ReadString();
            var pass = buffer.ReadString();

            user = Program.EKeyPair.DecryptString(user);
            pass = Program.EKeyPair.DecryptString(pass);
            Console.WriteLine("User[" + index + "] logged in with: " + user + ", " + pass);
        }

        internal static void Handle_Message(int index, ref byte[] data)
        {
            // Passing an array in the new parameters starts the buffer with 
            // already existing data expected to be read.
            var buffer = new ByteStream(data);

            // Unlike the send functions, we dont read for the header
            // that we always have to write first. The network object
            // already strips that off because it needed to know the 
            // header already to tell PacketID which pointer to invoke.
            // NOTE: Our packetrouter is assigning functions to the array
            // values in PacketID, that is why if a header is invoked it knows
            // which function to call.
            var msg = buffer.ReadString();

            // Cleanup
            buffer.Dispose();

            // This is really only here to help you see the clients message
            // did in fact make it in case you have a problem youll know if
            // the client sent a message but it didnt reach the server or the
            // server sent the message back to client but the client just didnt
            // receive it.
            Console.WriteLine(msg);

            // Sends the message to all connected clients!
            SendRelayMessage(msg);
        }
    }
}
