﻿using Asfw.IO.Encryption;
using System;
using System.IO;

namespace server
{
    static class Program
    {

        internal static KeyPair EKeyPair = new KeyPair();

        public static void Main(string[] args)
        {
            // LOAD ENCRYPTION
            var f = Environment.CurrentDirectory + "/Keys.xml";
            if (!File.Exists(f))
            {
                EKeyPair.GenerateKeys();
                EKeyPair.ExportKey(f, true); // True exports private key too.
                                             // Remember never pass private to client!
            }
            else EKeyPair.ImportKey(f);
            // END LOAD ENCRYPTION

            Network.InitNetwork();
            Network.StartListening(4000, 5);

            Console.WriteLine("Server ready for connections!");

            var running = true;
            while (running)
            {
                var command = Console.ReadLine().Trim();
                switch (command.ToLower())
                {
                    case "/exit": running = false; break;
                    default: break;
                }
            }

            Network.StopListening();
            Network.DestroyNetwork();
            Environment.Exit(0);
        }
    }
}
