﻿using System;
using System.IO;

namespace Asfw
{
    public static class OS
    {
        private static bool _initialized;

        private static void Activate()
        {
            if (_initialized) return;

            if (IntPtr.Size == 8) _isBit64 = true;
            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Unix:
                    if (Directory.Exists("/Applications") &&
                        Directory.Exists("/System") &&
                        Directory.Exists("/Users") &&
                        Directory.Exists("/Volumes"))
                        _isMac = true;
                    else
                        _isLinux = true;
                    break;
                case PlatformID.MacOSX:
                    _isMac = true;
                    break;
                case PlatformID.Win32Windows:
                case PlatformID.Win32NT:
                case PlatformID.Win32S:
                case PlatformID.WinCE:
                    _isWindows = true;
                    break;
                case PlatformID.Xbox:
                    _isXBox = true;
                    break;
                default:
                    // Assume its running on mobile.
                    _isMobile = true;
                    break;
            }

            _initialized = true;
        }

        private static bool _isMobile;

        public static bool Mobile
        {
            get
            {
                Activate();
                return _isMobile;
            }
        }

        private static bool _isXBox;

        public static bool XBox
        {
            get
            {
                Activate();
                return _isXBox;
            }
        }

        private static bool _isWindows;

        public static bool Windows
        {
            get
            {
                Activate();
                return _isWindows;
            }
        }

        private static bool _isMac;

        public static bool Mac
        {
            get
            {
                Activate();
                return _isMac;
            }
        }

        private static bool _isLinux;

        public static bool Linux
        {
            get
            {
                Activate();
                return _isLinux;
            }
        }

        private static bool _isBit64;

        public static bool X86
        {
            get
            {
                Activate();
                return !_isBit64;
            }
        }

        public static bool X64
        {
            get
            {
                Activate();
                return _isBit64;
            }
        }
    }
}