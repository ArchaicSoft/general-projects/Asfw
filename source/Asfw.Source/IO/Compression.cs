﻿using System;
using System.IO;
using System.IO.Compression;
using System.Threading.Tasks;

namespace Asfw.IO
{
    public static class Compression
    {
        /// <summary> Compresses byte-array data. </summary>
        public static byte[] CompressBytes(byte[] value)
        {
            var len = value.Length;
            using (var ms = new MemoryStream())
            {
                using (var gs = new GZipStream(ms, CompressionMode.Compress))
                    gs.Write(value, 0, len);

                return ms.ToArray();
            }
        }

        /// <summary> Compresses byte-array data asynchronously. </summary>
        public static async Task<byte[]> CompressBytesAsync(byte[] value)
        {
            var len = value.Length;
            using (var ms = new MemoryStream())
            {
                using (var gs = new GZipStream(ms, CompressionMode.Compress))
                    await gs.WriteAsync(value, 0, len);

                return ms.ToArray();
            }
        }

        /// <summary> Compresses specified block of byte-array data. </summary>
        public static byte[] CompressBytes(byte[] value, int offset, int size)
        {
            using (var ms = new MemoryStream())
            {
                using (var gs = new GZipStream(ms, CompressionMode.Compress))
                    gs.Write(value, offset, size);

                return ms.ToArray();
            }
        }

        /// <summary> Compresses specified block of byte-array data asynchronously. </summary>
        public static async Task<byte[]> CompressBytesAsync(byte[] value,
            int offset, int size)
        {
            using (var ms = new MemoryStream())
            {
                using (var gs = new GZipStream(ms, CompressionMode.Compress))
                    await gs.WriteAsync(value, offset, size);

                return ms.ToArray();
            }
        }

        /// <summary> 
        /// Returns a byte-array of specified 
        /// file after being compressed. 
        /// </summary>
        public static byte[] CompressFile(string path)
        {
            var buffer = File.ReadAllBytes(path);
            var len = buffer.Length;

            using (var ms = new MemoryStream())
            {
                using (var gs = new GZipStream(ms, CompressionMode.Compress))
                    gs.Write(buffer, 0, len);

                return ms.ToArray();
            }
        }

        /// <summary> 
        /// Returns a byte-array of specified 
        /// file after being compressed asynchronously. 
        /// </summary>
        public static async Task<byte[]> CompressFileAsync(string path)
        {
            var buffer = File.ReadAllBytes(path);
            var len = buffer.Length;

            using (var ms = new MemoryStream())
            {
                using (var gs = new GZipStream(ms, CompressionMode.Compress))
                    await gs.WriteAsync(buffer, 0, len);

                return ms.ToArray();
            }
        }

        /// <summary> 
        /// Compresses specified file and re-saves it as
        /// specified destination file. 
        /// </summary>
        public static void CompressFile(string srcFile, string dstFile)
        {
            File.WriteAllBytes(dstFile, CompressFile(srcFile));
        }

        /// <summary> 
        /// Compresses specified file and re-saves it as
        /// specified destination file asynchronously. 
        /// </summary>
        public static async Task CompressFileAsync(string srcFile, string dstFile)
        {
            File.WriteAllBytes(dstFile, await CompressFileAsync(srcFile));
        }

        /// <summary> Decompresses byte-array data. </summary>
        public static byte[] DecompressBytes(byte[] value)
        {
            var len = BitConverter.ToInt32(value, value.Length - 4);
            var buffer = new byte[len];

            using (var ms = new MemoryStream(value))
            using (var gs = new GZipStream(ms, CompressionMode.Decompress))
                gs.Read(buffer, 0, len);

            return buffer;
        }

        /// <summary> Decompresses byte-array data asynchronously. </summary>
        public static async Task<byte[]> DecompressBytesAsync(byte[] value)
        {
            var len = BitConverter.ToInt32(value, value.Length - 4);
            var buffer = new byte[len];

            using (var ms = new MemoryStream(value))
            using (var gs = new GZipStream(ms, CompressionMode.Decompress))
                await gs.ReadAsync(buffer, 0, len);

            return buffer;
        }

        /// <summary> Decompresses specified block of byte-array data. </summary>
        public static byte[] DecompressBytes(byte[] value, int offset, int size)
        {
            var buffer = new byte[size];
            Buffer.BlockCopy(value, offset, buffer, 0, size);

            return DecompressBytes(buffer);
        }

        /// <summary> Decompresses specified block of byte-array data asynchronously. </summary>
        public static async Task<byte[]> DecompressBytesAsync(byte[] value,
            int offset, int size)
        {
            var buffer = new byte[size];
            Buffer.BlockCopy(value, offset, buffer, 0, size);

            return await DecompressBytesAsync(buffer);
        }

        /// <summary> 
        /// Returns a byte-array of specified 
        /// file after being decompressed. 
        /// </summary>
        public static byte[] DecompressFile(string path)
        {
            var value = File.ReadAllBytes(path);
            var len = BitConverter.ToInt32(value, value.Length - 4);
            var buffer = new byte[len];

            using (var ms = new MemoryStream(value))
            using (var gs = new GZipStream(ms, CompressionMode.Decompress))
                gs.Read(buffer, 0, len);

            return buffer;
        }

        /// <summary> 
        /// Returns a byte-array of specified 
        /// file after being decompressed asynchronously. 
        /// </summary>
        public static async Task<byte[]> DecompressFileAsync(string path)
        {
            var value = File.ReadAllBytes(path);
            var len = BitConverter.ToInt32(value, value.Length - 4);
            var buffer = new byte[len];

            using (var ms = new MemoryStream(value))
            using (var gs = new GZipStream(ms, CompressionMode.Decompress))
                await gs.ReadAsync(buffer, 0, len);

            return buffer;
        }

        /// <summary> 
        /// Decompresses specified file and re-saves it as
        /// specified destination file. 
        /// </summary>
        public static void DecompressFile(string srcFile, string dstFile)
        {
            File.WriteAllBytes(dstFile, DecompressFile(srcFile));
        }

        /// <summary> 
        /// Decompresses specified file and re-saves it as
        /// specified destination file asynchronously. 
        /// </summary>
        public static async Task DecompressFileAsync(string srcFile, string dstFile)
        {
            File.WriteAllBytes(dstFile, await DecompressFileAsync(srcFile));
        }
    }
}